# appMovil

### Requirements

1. Docker
2. Docker-compose

## How to run it?

1. Clone the repository:
```
git clone https://gitlab.com/reyesch/appmovil.git
```

2. Go to the cloned repository:
```
cd appmovil
```

3. Build the application
```
$ docker-compose build
```

4. Apply Django migrations
```
$ docker-compose run --rm api python3 manage.py migrate
```

5. Run the application
```
$ docker-compose up
```

## Where is the application running?
- AppMovil Core (API) - http://localhost:8000
- Mailhog (Test email interface)- http://localhost:8025
