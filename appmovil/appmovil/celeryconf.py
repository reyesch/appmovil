import os
import logging

from celery import Celery
from celery.signals import setup_logging
from django.conf import settings

# from .plugins import discover_plugins_modules

CELERY_LOGGER_NAME = "celery"

@setup_logging.connect
def setup_celery_logging(loglevel=None, **kwargs):
    if loglevel:
        logging.getLogger(CELERY_LOGGER_NAME).setLevel(loglevel)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "appmovil.settings")

app = Celery("appmovil")

app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
# app.autodiscover_tasks(lambda: discover_plugins_modules(settings.PLUGINS))