from django.db import models
from django.utils.translation import gettext_lazy as _

from ..account.models import User

class Idea(models.Model):

    class Visibility(models.TextChoices):
        PUBLIC = 'PB', _('public')
        PROTECTED = 'PT', _('protected')
        PRIVATE = 'PV', _('private')

    user = models.ForeignKey(User, related_name="idea_user", blank=False, null=False, on_delete=models.CASCADE)
    content = models.TextField(blank=False, null=False)
    visibility = models.CharField(
        max_length=2,
        choices=Visibility.choices,
        default=Visibility.PUBLIC
    )
    published_at = models.DateTimeField(auto_now_add=True, null=False)

    class Meta:
        ordering = ("published_at",)

    def __str__(self) -> str:
        return self.content