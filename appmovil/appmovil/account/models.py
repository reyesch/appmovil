from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    username = models.CharField(max_length=40, null=True, unique=True)
    follows = models.ManyToManyField(
        "self",
        related_name="users_follows",
        symmetrical=False,
        blank = True,
    )
    follow_requests = models.ManyToManyField(
        "self",
        related_name="users_requests",
        symmetrical=False,
        blank = True,
    )

    class Meta:
        ordering = ("email",)
