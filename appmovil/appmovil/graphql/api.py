import graphene

from .account.schema import AccountMutations, AccountQueries
from .idea.schema import IdeaMutations, IdeaQueries


class Query(
    AccountQueries,
    IdeaQueries,
    graphene.ObjectType,
):
    pass

class Mutation(
    AccountMutations,
    IdeaMutations,
    graphene.ObjectType,
):
    pass

schema = graphene.Schema(query=Query, mutation=Mutation)
