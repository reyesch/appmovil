from ...account.models import User


def resolver_user(info, user_pk):
    return User.objects.filter(pk = user_pk).first()


def resolve_follows(user:User):
    return user.follows.all()


def resolve_followers(user:User):
    return User.objects.filter(follows = user)


def resolve_follow_requests(user:User):
    return user.follow_requests.all()