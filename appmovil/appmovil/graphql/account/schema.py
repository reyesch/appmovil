import graphene
import graphql_jwt
from django.contrib.auth import get_user_model

from .mutations import (
    AcceptFollowRequest, 
    AccountRegister, 
    ChangePassword,
    DeclineFollowRequest, 
    RemoveFollower, 
    RequestFollow,
    RequestPasswordReset, 
    SetPassword, 
    Unfollow)
from .resolvers import resolver_user
from .types import UserType


class AccountQueries(graphene.ObjectType):
    me = graphene.Field(UserType, description="Information of the authenticated user.")
    user = graphene.Field(
            UserType,
            id=graphene.Argument(graphene.ID, description="ID of the user.", required=True),
            description="User by ID."
    )
    users = graphene.List(
        UserType,
        description="List of users."
    )

    def resolve_me(self, info):
        user = info.context.user
        return user if user.is_authenticated else None

    def resolve_user(self, info, id):
        return resolver_user(info, id)

    def resolve_users(self, info):
        return get_user_model().objects.all()


class AccountMutations(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

    account_register = AccountRegister.Field()
    set_password = SetPassword.Field()
    request_password_reset = RequestPasswordReset.Field()
    change_password = ChangePassword.Field()

    request_follow = RequestFollow.Field()
    accept_follow_request = AcceptFollowRequest.Field()
    decline_follow_request = DeclineFollowRequest.Field()
    remove_follower = RemoveFollower.Field()
    unfollow = Unfollow.Field()



    


