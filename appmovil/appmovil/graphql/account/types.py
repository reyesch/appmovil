import graphene
from graphene import relay
from django.core.exceptions import ValidationError
from graphene_django import DjangoObjectType

from ...account.models import User
from ..account.resolvers import resolve_followers, resolve_follows, resolve_follow_requests
from ..idea.resolvers import resolve_me_ideas, resolve_user_ideas
from ..idea.types import IdeaType


class UserType(DjangoObjectType):
    follows = graphene.List(lambda: UserType,
        description="List of all the users that the account follows")
    followers = graphene.List(lambda: UserType,
        description="List of the followers")
    follow_requests = graphene.List(lambda: UserType,
        description="List of all the follow requests.")
    ideas = graphene.List(IdeaType, description='List of all the ideas of the user')

    class Meta:
        description = "User data"
        model = User
        only_fields = (
            "id",
            "username",
        )

    @staticmethod
    def resolve_follows(root: User, __info, **kwargs):
        return resolve_follows(root)

    @staticmethod
    def resolve_followers(root: User, __info, **kwargs):
        return resolve_followers(root)

    @staticmethod
    def resolve_ideas(root: User, info, **kwargs):
        user = info.context.user
        if root == user:
            return resolve_me_ideas(user)
        else:
            return resolve_user_ideas(user, root)

    @staticmethod
    def resolve_follow_requests(root: User, info, **kwargs):
        user = info.context.user
        if root == user:
            return resolve_follow_requests(user)
        else:
            raise PermissionError(
            {
            "idea": ValidationError(
                "You don't have permission to view this information."
                )
            }
        )
