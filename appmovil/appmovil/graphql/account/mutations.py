import graphene
from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from graphql_jwt import ObtainJSONWebToken

from ...account.models import User


class AccountRegister(graphene.Mutation):
    username = graphene.String(description="Username of a user.")
    email = graphene.String(description="Email of a user.")

    class Arguments:
        username = graphene.String(required=True, description="Username of a user.")
        email = graphene.String(required=True, description="Email of a user.")
        password = graphene.String(required=True, description="Password of a user.")

    @classmethod
    def mutate(cls, root, info, **data):
        username = data["username"]
        email = data["email"]
        password = data["password"]
        user = get_user_model()(
            username=username,
            email=email,
        )
        user.set_password(password)
        user.save()

    

class SetPassword(ObtainJSONWebToken):
    token = graphene.String(description="JWT token")

    class Arguments:
        token = graphene.String(description="JWT token", 
        
        
         required=True)
        email = graphene.String(required=True, description="Email of a user.")
        password = graphene.String(required=True, description="Password of a user.")

    @classmethod
    def mutate(cls, root, info, **data):
        token = data["token"]
        email = data["email"]
        password = data["password"]
        try:
            user = User.objects.get(email = email)
        except ObjectDoesNotExist:
            raise ValidationError(
                {
                    "email": ValidationError(
                        "User doesn't exist"
                    )
                }
            )
        if not default_token_generator.check_token(user, token):
            raise ValidationError(
                {
                    "token": ValidationError("Invalid token.")
                }
            )
        try:
            password_validation.validate_password(password, user)
        except ValidationError as error:
            raise ValidationError({"password": error})
        user.set_password(password)
        user.save(update_fields=["password"])


class RequestPasswordReset(graphene.Mutation):
    email = graphene.String(description="Email of a user.")
    redirect_url = graphene.String(description="URL.")

    class Arguments:
        email = graphene.String(required=True, description="Email of a user.")
        redirect_url = graphene.String(required=True, description="URL.")
  
    @classmethod
    def mutate(cls, root, info, **data):
        email = data["email"]
        try:
            user = User.objects.get(email = email)
        except ObjectDoesNotExist:
            raise ValidationError(
                    {
                        "email": ValidationError(
                            "User doesn't exist."
                        )
                    } 
            )
        # send_password_reset_email(redirect_url, user)
        return RequestPasswordReset()


class ChangePassword(graphene.Mutation):
    user = graphene.String(description="User")
    
    class Arguments:
        old_password = graphene.String(required=True, description="Current password of the user.")
        new_password = graphene.String(required=True, description="New password.")
    
    @classmethod
    def mutate(cls, root, info, **data):
        user = info.context.user
        old_password = data["old_password"]
        new_password = data["new_password"]
        if not user.is_authenticated:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "User is not authenticated."
                    )
                }
            )
        else:
            if not user.check_password(old_password):
                raise ValidationError(
                    {
                        "old_password": ValidationError(
                            "Old password is not valid."
                        )
                    }
                )
            try:
                password_validation.validate_password(new_password, user)
            except ValidationError as error:
                raise ValidationError({"new_password": error})
            
            user.set_password(new_password)
            user.save(update_field=["password"])
            return ChangePassword(user = user)


class RequestFollow(graphene.Mutation):
    username = graphene.String(description="Username of a user to follow.")

    class Arguments:
        id = graphene.ID(required=True, description="ID of user to follow.")
        

    @classmethod
    def mutate(cls, root, info, **data):
        account_id = data["id"]
        user = info.context.user
        try:
            account = User.objects.get(id = account_id)
        except ObjectDoesNotExist:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "Requested user does not exist."
                    )
                }
            )
        if account == user:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "You can't follow yourself."
                    )
                }
            )
        if account in user.follows.all():
            raise ValidationError(
                {
                    "user": ValidationError(
                        "You follow this user."
                    )
                }
            )
        elif user in account.follow_requests.all():
            raise ValidationError(
                {
                    "user": ValidationError(
                        "You have requested to follow this user."
                    )
                }
            )
        else:
            account.follow_requests.add(user)
            account.save()


class AcceptFollowRequest(graphene.Mutation):
    username = graphene.String(description="List of the users pending to accept.")

    class Arguments:
        id = graphene.ID(required=True, description="ID of user to accept.")
        

    @classmethod
    def mutate(cls, root, info, **data):
        account_id = data["id"]
        user = info.context.user
        try:
            account = User.objects.get(id = account_id)
        except ObjectDoesNotExist:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "Requested user does not exist."
                    )
                }
            )
        if not account in user.follow_requests.all():
            raise ValidationError(
                {
                    "user": ValidationError(
                        "This user is not in the follow requests list."
                    )
                }
            )
        else:
            account.follows.add(user)
            user.follow_requests.remove(account)
            account.save()
            user.save()


class DeclineFollowRequest(graphene.Mutation):
    username = graphene.String(description="List of the users pending to accept.")

    class Arguments:
        id = graphene.ID(required=True, description="ID of user to decline.")
        

    @classmethod
    def mutate(cls, root, info, **data):
        account_id = data["id"]
        user = info.context.user
        try:
            account = User.objects.get(id = account_id)
        except ObjectDoesNotExist:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "Requested user does not exist."
                    )
                }
            )
        if not account in user.follow_requests.all():
            raise ValidationError(
                {
                    "user": ValidationError(
                        "This user is not in the follow requests list."
                    )
                }
            )
        else:
            user.follow_requests.remove(account)
            user.save()

        
class RemoveFollower(graphene.Mutation):
    username = graphene.String(description="List of the users pending to accept.")

    class Arguments:
        id = graphene.ID(required=True, description="ID of user to remove.")
        

    @classmethod
    def mutate(cls, root, info, **data):
        account_id = data["id"]
        user = info.context.user
        try:
            account = User.objects.get(id = account_id)
        except ObjectDoesNotExist:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "Requested user does not exist."
                    )
                }
            )
        if not user in account.follows.all():
            raise ValidationError(
                {
                    "user": ValidationError(
                        "This user doesn't follow you."
                    )
                }
            )
        else:
            account.follows.remove(user)
            account.save()


class Unfollow(graphene.Mutation):
    username = graphene.String(description="List of the users pending to accept.")

    class Arguments:
        id = graphene.ID(required=True, description="ID of user to unfollow.")
        

    @classmethod
    def mutate(cls, root, info, **data):
        account_id = data["id"]
        user = info.context.user
        try:
            account = User.objects.get(id = account_id)
        except ObjectDoesNotExist:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "Requested user does not exist."
                    )
                }
            )
        if not account in user.follows.all():
            raise ValidationError(
                {
                    "user": ValidationError(
                        "This user is not in the follows list."
                    )
                }
            )
        else:
            user.follows.remove(account)
            user.save()