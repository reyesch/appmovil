from ...idea.models import Idea

def is_user_idea(user, idea_id):
    if not user.is_authenticated:
        return False
    return Idea.objects.get(pk = idea_id).user == user