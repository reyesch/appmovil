from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.models import Q

from ...account.models import User
from ...idea.models import Idea


def resolve_idea(info, idea_id):
    user = info.context.user
    try:
        idea = Idea.objects.get(id = idea_id)
    except ObjectDoesNotExist:
        raise ValidationError(
            {
            "idea": ValidationError(
                "Idea doesn't exist"
                )
            }
        )  
    if (idea.visibility == 'PB' or 
    (idea.visibility == 'PT' and idea.user in user.follows.all()) or
    idea.user == user):
        return idea
    else:
        raise PermissionError(
            {
            "idea": ValidationError(
                "You don't have permission to view this idea."
                )
            }
        )


def resolve_me_ideas(user:User):
    return Idea.objects.filter(user=user).order_by('-published_at')


def resolve_user_ideas(user:User, account_id):
    try:
        account = User.objects.get(id = account_id)
    except ObjectDoesNotExist:
        raise ValidationError(
            {
            "user": ValidationError(
                "User doesn't exist"
                )
            }
        )
    if user.is_anonymous or not account in user.follows.all():
        ideas = Idea.objects.filter(user=account, visibility='PB')
    elif user == account:
        ideas = Idea.objects.filter(user=account)
    else:
        ideas = Idea.objects.filter(Q(visibility='PT') | Q(visibility='PB'), user=account) 
    return ideas.order_by('-published_at')


def resolve_timeline(info):
    user = info.context.user
    if user.is_anonymous:
        ideas = Idea.objects.filter(visibility='PB')
    else:
        ideas = Idea.objects.filter(
            Q(user = user) |
            Q(user_id__in = user.follows.all(), visibility='PT') | 
            Q(visibility='PB')
            )
    return ideas.order_by('-published_at')
