import graphene
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from graphene import Enum

from ...idea.models import Idea
from ..idea.utils import is_user_idea
from .types import IdeaType


class IdeaVisibilityEnum(Enum):
    PUBLIC = "PB"
    PROTECTED = "PT"
    PRIVATE = "PV"


class CreateIdea(graphene.Mutation):
    id = graphene.Int()

    class Arguments:
        content = graphene.String(required=True, description  = "Content of the idea.")
        visibility = graphene.Argument(
            IdeaVisibilityEnum, 
            required=False, 
            description="Determine the visibiity of the idea."
            )

    @classmethod
    def mutate(cls, root, info, **data):
        user = info.context.user
        content = data["content"]
        visibility = data["visibility"]
        if not user.is_authenticated:
            raise ValidationError(
                {
                    "user": ValidationError(
                        "User is not authenticated."
                    )
                }
            )
        else:
            instance = Idea(user=user, content=content, visibility=visibility)
            instance.save()


class UpdateIdeaVisibility(graphene.Mutation):
    id = graphene.Int()
    content = graphene.String()
    visibility = graphene.String()

    class Arguments:
        idea = graphene.ID(required=True, description = "ID of the idea.")
        visibility = graphene.Argument(
            IdeaVisibilityEnum, 
            required=True, 
            description="Determine the visibiity of the idea."
            )
    
    
    @classmethod
    def mutate(cls, root, info, **data):
        user = info.context.user
        visibility = data["visibility"]
        if not is_user_idea(user, data["idea"]):
            raise ValidationError(
                {
                    "idea": ValidationError(
                        "The idea is not from the user."
                    )
                }
            )
        idea = Idea.objects.get(id = data["idea"])
        idea.visibility=visibility
        idea.save()


    
