import graphene

from .mutations import CreateIdea, UpdateIdeaVisibility
from .resolvers import resolve_idea, resolve_timeline, resolve_user_ideas
from .types import IdeaType


class IdeaQueries(graphene.ObjectType):
    idea = graphene.Field(
        IdeaType,
        id=graphene.Argument(graphene.ID, description="ID of the idea.", required=True),
        description="Idea by ID."
    )
    user_ideas = graphene.List(
        IdeaType,
        account = graphene.Argument(graphene.ID, description="Id of the account requested.", required=True),
        description="List of ideas."
    )
    timeline = graphene.List(
        IdeaType,
        description="User timeline."
    )

    def resolve_idea(self, info, id):
        return resolve_idea(info, id)

    def resolve_user_ideas(self, info, account):
        user = info.context.user
        return resolve_user_ideas(user, account)

    def resolve_timeline(self, info):
        return resolve_timeline(info)


class IdeaMutations(graphene.ObjectType):
    create_idea = CreateIdea.Field()
    update_idea_visibility = UpdateIdeaVisibility.Field()
