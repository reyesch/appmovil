import graphene
from graphene import relay
from graphene_django import DjangoObjectType

from ...idea.models import Idea

class IdeaType(DjangoObjectType):
    content = graphene.String(
        description ="Content of the idea.", 
        )
    id = graphene.ID(description = "ID of the idea")

    class Meta:
        description = "Defines user's idea."
        model = Idea
        only_fields = (
            "visibility",
            "published_at",
            "user"
        )
