"""appmovil URL Configuration"""

from django.conf import settings
from django.urls import path
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt

from graphene_django.views import GraphQLView

urlpatterns = [path('', csrf_exempt(GraphQLView.as_view(graphiql=True)))]
